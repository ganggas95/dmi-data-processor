#!/bin/bash
VENV_DIR=/Users/nizar/Library/Caches/pypoetry/virtualenvs/geo-extractor-gZT3zHNM-py3.11
PROJECT_DIR=/Users/nizar/chisu/geo_extractor
set -e
cd $PROJECT_DIR
source .env
source $VENV_DIR/bin/activate
python main.py calculate-rmi

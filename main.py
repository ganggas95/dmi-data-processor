# Flake8: noqa
import os
import sys
from dotenv import load_dotenv

load_dotenv()

current: str = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(current, "src"))


from src.factory import cli

if __name__ == "__main__":
    cli()

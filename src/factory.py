# Flake8: noqa; pylint: disable=unused-import
import os
import click
from dotenv import load_dotenv
from container import Container
from commands.org_unit_command import org_unit_commands
from commands.calculate_rmi import rmi_commands
from commands.user_command import user_commands

dotenv_path = os.path.join(os.path.dirname(__file__), ".env")
load_dotenv(dotenv_path)


app = Container()
cli = click.CommandCollection(sources=[org_unit_commands, rmi_commands, user_commands])

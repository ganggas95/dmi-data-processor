from dependency_injector import providers
from dependency_injector.containers import (DeclarativeContainer,
                                            WiringConfiguration)

from db import Database
from repositories.data_element_repository import DataElementRepository
from repositories.data_value_repository import DataValueRepository
from repositories.dataset_repository import DataSetRepository
from repositories.org_unit_repository import OrgUnitRepository
from repositories.period_repository import PeriodRepository
from repositories.user_repository import UserRepository
from services.calculate_rmi_service import CalculateRMIService
from services.orgunit_service import OrgUnitService
from services.user_service import UserService


class Container(DeclarativeContainer):
    wiring_config = WiringConfiguration(
        modules=[
            "repositories",
            "services",
            "commands",
            "commands.org_unit_command",
            "commands.calculate_rmi",
            "commands.user_command",
        ],
        packages=["internal"],
    )

    config = providers.Configuration()

    db = providers.Singleton(Database)
    db_session = providers.Singleton(db.provided.session)
    period_repository = providers.Factory(PeriodRepository, session=db.provided.session)
    dataset_repository = providers.Factory(
        DataSetRepository, session=db.provided.session
    )
    data_element_repository = providers.Factory(
        DataElementRepository, session=db.provided.session
    )
    data_value_repository = providers.Factory(
        DataValueRepository,
        session=db.provided.session,
        data_element_repository=data_element_repository,
    )
    orgunit_repository = providers.Factory(
        OrgUnitRepository, session=db.provided.session
    )
    orgunit_service = providers.Factory(
        OrgUnitService, orgunit_repository=orgunit_repository
    )
    user_repository = providers.Factory(UserRepository, session=db.provided.session)
    user_service = providers.Factory(
        UserService,
        user_repository=user_repository,
        org_unit_repository=orgunit_repository,
    )
    calculate_rmi_service = providers.Factory(
        CalculateRMIService,
        data_element_repository=data_element_repository,
        data_value_repository=data_value_repository,
        period_repository=period_repository,
    )

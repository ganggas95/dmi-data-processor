DATA_ELEMENT_RMI_SHORTNAMES = [
    "VII.C.1.2",
    "VII.C.1.3",
    "VII.C.2.2-New",
    "VII.C.2.8-New",
    "VII.C.3.2",
    "VII.C.4.2",
    "I.A.1",
    "VII.C.3.3",
    "VII.C.5.2",
    "VII.C.4.3",
    "VII.C.4.8",
    "VII.C.5.4",
    "VII.C.2.6",
    "VII.C.3.6",
    "VII.C.3.9",
    "VII.C.4.7",
    "VII.C.3.10",
    "VII.D.1",
    "VII.C.2.11",
    "VII.C.4.10",
    "VII.C.4.11",
]


RMI_INDEX_VALUE_SUMMARY = {
    "0-1": 2,
    "2-5": 4,
    "6-8": 5,
    "9-11": 3,
    "12-15": 4,
    "16-17": 6,
    "18-20": 6,
}


MAP_DATA_ELEMENT_RMI = {
    "0": "o6ze0eZOnmt",
    "1": "tuHlhNFGqHQ",
    "2": "BJ7vsw7IFNC",
    "3": "UVtK6VTGiMC",
    "4": "Tfr6ixkyxxK",
    "5": "wvMw5ckgKgC",
    "6": "CUqqBlYJzlg",
    "7": "uZflOOvzpyb",
}

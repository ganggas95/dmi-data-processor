import os
from functools import cached_property

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class Database:
    def __init__(self):
        self.engine = create_engine(
            os.getenv("DATABASE_URL"),
            connect_args={
                "password": os.getenv("DATABASE_PASSWORD"),
            },
        )

    @cached_property
    def session(self):
        Session = sessionmaker(bind=self.engine)
        return Session()

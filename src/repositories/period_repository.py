from functools import cached_property
from typing import List, Union

from sqlalchemy import extract
from sqlalchemy.orm import Query

from models.period import Period, PeriodType

from .base_repository import BaseRepository


class PeriodRepository(BaseRepository):
    model = Period

    @cached_property
    def query_period_type(self) -> Query:
        return self.session.query(PeriodType)

    def all(self) -> List[Period]:
        return self.query.all()

    def get_period_types(self) -> List[PeriodType]:
        return self.query_period_type.all()

    def get_by_id(self, period_id: int) -> Union[Period, None]:
        return self.query.get(period_id)

    def get_period_type(self, period_type: str) -> Union[PeriodType, None]:
        return self.query_period_type.filter(PeriodType.name == period_type).first()

    def get_by_year(self, year: int) -> Union[Period, None]:
        yearly = self.get_period_type("Yearly")
        if yearly is None:
            return None
        return self.query.filter(
            Period.periodtypeid == yearly.periodtypeid,
            extract("year", Period.startdate) == year,
        ).first()

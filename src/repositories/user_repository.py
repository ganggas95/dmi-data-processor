from typing import Union
from sqlalchemy.orm import Query
from .base_repository import BaseRepository
from models.userinfo import UserInfo, UserRole


class UserRepository(BaseRepository):
    model = UserInfo

    def all(self):
        return self.query.all()

    def query_userrole(self) -> Query:
        return self.session.query(UserRole)

    def get_userrole_by_uid(self, uid: str) -> Union[UserRole, None]:
        return self.query_userrole().filter(UserRole.uid == uid).first()

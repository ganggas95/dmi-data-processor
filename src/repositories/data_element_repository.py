from typing import List, Union

from constants.rmi_const import MAP_DATA_ELEMENT_RMI
from models.data_element import DataElement

from .base_repository import BaseRepository


class DataElementRepository(BaseRepository):
    model = DataElement

    def all(self) -> List[DataElement]:
        return self.query.limit(2).all()

    def get_by_id(self, data_element_id) -> DataElement:
        return self.query.get(data_element_id)

    def get_by_uid(self, data_element_uid) -> Union[DataElement, None]:
        return self.query.filter(DataElement.uid == data_element_uid).first()

    def get_data_element_rme_by_level(self, level: int) -> DataElement:
        if str(level) not in MAP_DATA_ELEMENT_RMI:
            raise Exception("RME level not found")
        rme_uid = MAP_DATA_ELEMENT_RMI[str(level)]
        return self.get_by_uid(rme_uid)

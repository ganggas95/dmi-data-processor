# from models.data_element import DataElement, DataElementGroup
# from models.data_value import DataValue
from models.dataset_element import DataSet

from .base_repository import BaseRepository

# from models.period import Period


class DataSetRepository(BaseRepository):
    model = DataSet

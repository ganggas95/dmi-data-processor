from typing import List, Union

from models.org_unit import OrgUnit
from src.repositories.base_repository import BaseRepository


class OrgUnitRepository(BaseRepository):
    model = OrgUnit

    def all(self) -> List[OrgUnit]:
        return self.query.filter(
            OrgUnit.hierarchylevel == 4
        ).all()

    def get_by_id(self, org_unit_id: int) -> Union[OrgUnit, None]:
        return self.query.filter(OrgUnit.organisationunitid == org_unit_id).first()

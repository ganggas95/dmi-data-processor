from typing import Any
from functools import cached_property
from sqlalchemy.orm import Query
from internal.types import ScopedSession


class BaseRepository:
    model = None

    def __init__(self, session: ScopedSession):
        self.session = session
        if self.model is None:
            raise NotImplementedError("model must be set")

    @cached_property
    def query(self) -> Query:
        return self.session.query(self.model)

    def save(self, instance: Any):
        self.session.add(instance)

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()
        self.session.close_all()

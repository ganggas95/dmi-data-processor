from datetime import datetime
from typing import List, Union

from sqlalchemy import extract, or_

from internal.types import ScopedSession
from models.data_element import DataElement
from models.data_value import DataValue
from models.dataset_element import DataSet, DataSetElement
from models.period import Period

from .base_repository import BaseRepository
from .data_element_repository import DataElementRepository


class DataValueRepository(BaseRepository):
    model = DataValue

    def __init__(
        self, session: ScopedSession, data_element_repository: DataElementRepository
    ):
        super().__init__(session)
        self._data_element_repository = data_element_repository

    def find_rme_data_values(
        self, level: int, source_id: int, period_id: int
    ) -> Union[DataValue, None]:
        data_element = self._data_element_repository.get_data_element_rme_by_level(
            level
        )
        return (
            self.query.join(
                Period,
                DataValue.periodid == Period.periodid,
            )
            .join(DataElement, DataElement.dataelementid == DataValue.dataelementid)
            .join(
                DataSetElement,
                DataSetElement.dataelementid == DataElement.dataelementid,
            )
            .filter(
                DataValue.periodid == period_id,
                DataValue.sourceid == source_id,
                DataValue.dataelementid == data_element.dataelementid,
            )
            .first()
        )

    def get_by_source_id(self, source_id: int) -> List[DataValue]:
        return (
            self.query.join(
                Period,
                DataValue.periodid == Period.periodid,
            )
            .join(DataElement, DataElement.dataelementid == DataValue.dataelementid)
            .join(
                DataSetElement,
                DataSetElement.dataelementid == DataElement.dataelementid,
            )
            .filter(
                DataSetElement.dataset.has(
                    DataSet.shortname.in_(["MIKRO-RME", "MIKRO"])
                ),
                or_(
                    DataElement.name.like("VII.C.%"),
                    DataElement.shortname.like("I.A.1"),
                    DataElement.shortname.like("VII.D.1"),
                ),
                extract("year", Period.startdate) == datetime.now().year,
                DataValue.sourceid == source_id,
            )
            .order_by(DataElement.shortname.asc())
            .all()
        )

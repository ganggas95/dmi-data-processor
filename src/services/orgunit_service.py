from typing import Union

from osgeo.ogr import Geometry

from models.org_unit import OrgUnit
from repositories.org_unit_repository import OrgUnitRepository


class OrgUnitService:
    def __init__(self, orgunit_repository: OrgUnitRepository) -> None:
        self.orgunit_repository = orgunit_repository

    def get_all_org_units(self):
        return self.orgunit_repository.all()

    def get_org_unit_by_code(self, code: str) -> Union[OrgUnit, None]:
        return self.orgunit_repository.query.filter_by(code=code).first()

    def update_geometry(self, org_unit: OrgUnit, geometry: Geometry):
        org_unit.geometry = geometry
        self.orgunit_repository.session.add(org_unit)

    def commit(self):
        self.orgunit_repository.session.commit()

    def rollback(self):
        self.orgunit_repository.session.rollback()

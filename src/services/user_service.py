import logging
from functools import cached_property
from typing import List

from sqlalchemy.orm import Query

from models.userinfo import (UserDataViewOrgUnits, UserGroup, UserGroupMembers,
                             UserInfo, UserRole, UserRoleAuthorities,
                             UserRoleMembers)
from repositories.org_unit_repository import OrgUnitRepository
from repositories.user_repository import UserRepository


class UserService:
    # Sesuaikan dengan user role admin
    role_superadmin = 2
    # ID dari org unit Konsensus
    konsensus_org_unit = 27703
    # ID dari org unit Indonesia
    indonesia_org_unit = 65

    M_BENCHMARK = "M_Benchmark"
    M_FEEDBACK = "M_Feedback"

    def __init__(
        self, user_repository: UserRepository, org_unit_repository: OrgUnitRepository
    ) -> None:
        self.user_repository = user_repository
        self.org_unit_repository = org_unit_repository

    @cached_property
    def query_with_user_group_join(self) -> Query:
        """
        Returns a query object with a join on user group tables.
        :return: The query object with the join.
        """
        # Join UserInfo table with UserGroupMembers table on userinfoid
        # Join UserInfo table with UserGroupMembers table on userinfoid

        user_group_join = self.user_repository.query.join(
            UserGroupMembers, UserGroupMembers.userinfoid == UserInfo.userinfoid
        )

        # Join UserGroup table with UserGroupMembers table on usergroupid
        user_group_join = user_group_join.join(
            UserGroup,
            UserGroup.usergroupid == UserGroupMembers.usergroupid,
        )
        return user_group_join

    @cached_property
    def query_with_user_role_join(self) -> Query:
        """
        Returns a query object with a join on user role tables.
        :return: The query object with the join.
        """
        # Join UserInfo table with UserRoleMembers table on userinfoid

        user_role_join = self.user_repository.query.join(
            UserRoleMembers, UserRoleMembers.userinfoid == UserInfo.userinfoid
        )

        # Join UserRole table with UserRoleMembers table on userroleid
        user_role_join = user_role_join.join(
            UserRole,
            UserRole.userroleid == UserRoleMembers.userroleid,
        )
        return user_role_join

    def get_all_users(self) -> List[UserInfo]:
        return self.user_repository.all()

    def get_all_user_makro_query(self) -> Query:
        return self.query_with_user_group_join.filter(UserGroup.uid == "uHcqKE2lkaj")

    def get_all_user_mikro_query(self) -> Query:
        return self.query_with_user_group_join.filter(UserGroup.uid == "xjGIkbSJsEC")

    def set_all_user_makro_to_have_konsensus(self):
        """
        Sets all user makro to have konsensus org unit, if they are not already connected to it and
        have access to view its data. Superadmins are excluded from this process.
        """
        # Get all user makro query
        query: List[UserInfo] = self.get_all_user_makro_query().all()
        # Get the konsensus org unit
        org_unit_konsensus = self.org_unit_repository.get_by_id(self.konsensus_org_unit)

        org_unit_indonesia = self.org_unit_repository.get_by_id(self.indonesia_org_unit)
        if org_unit_konsensus is None:
            raise Exception("Org unit konsensus not found")
        if org_unit_indonesia is None:
            raise Exception("Org unit konsensus not found")

        try:
            for user in query:
                # User Makro yang tidak terhubung dan memiliki aksess
                # untuk melihat data org unit konsensus
                # dan dia bukan merupakan superadmin
                user_dataviews = [
                    org_unit.id for org_unit in user.dataview_org_units
                ]
                user_roles = [group.userroleid for group in user.userroles]
                user_have_konsensus = self.konsensus_org_unit in user_dataviews
                user_have_indonesia = self.indonesia_org_unit in user_dataviews
                user_is_admin = self.role_superadmin in user_roles

                if not user_have_konsensus and not user_is_admin:
                    user_dataview_konsensus = UserDataViewOrgUnits(
                        userinfoid=user.userinfoid,
                        id=org_unit_konsensus.id,
                    )

                    self.user_repository.save(user_dataview_konsensus)
                if not user_have_indonesia and not user_is_admin:
                    user_dataview_indo = UserDataViewOrgUnits(
                        userinfoid=user.userinfoid,
                        id=org_unit_indonesia.id,
                    )
                    self.user_repository.save(user_dataview_indo)
                self.user_repository.commit()
        except Exception as e:
            logging.error(e)
            self.user_repository.rollback()

    def set_all_user_mikro_to_have_nasional(self):
        """
        Sets all user makro to have konsensus org unit, if they are not already connected to it and
        have access to view its data. Superadmins are excluded from this process.
        """
        # Get all user makro query
        query: List[UserInfo] = self.get_all_user_mikro_query().all()
        # Get the konsensus org unit
        org_unit_indonesia = self.org_unit_repository.get_by_id(self.indonesia_org_unit)

        if org_unit_indonesia is None:
            raise Exception("Org unit Indonesia not found")

        try:
            for user in query:
                # User Makro yang tidak terhubung dan memiliki aksess
                # untuk melihat data org unit konsensus
                # dan dia bukan merupakan superadmin
                user_dataviews = [
                    org_unit.id for org_unit in user.dataview_org_units
                ]
                user_roles = [group.userroleid for group in user.userroles]
                user_is_admin = self.role_superadmin in user_roles
                user_roles = [group.userroleid for group in user.userroles]
                user_is_admin = self.role_superadmin in user_roles
                user_have_indonesia = self.indonesia_org_unit in user_dataviews

                if not user_have_indonesia and not user_is_admin:
                    user_dataview_indo = UserDataViewOrgUnits(
                        userinfoid=user.userinfoid,
                        id=org_unit_indonesia.id,
                    )
                    self.user_repository.save(user_dataview_indo)
                    self.user_repository.commit()
        except Exception as e:
            logging.error(e)
            self.user_repository.rollback()

    def set_user_dmi_to_have_access_custom_app(self):
        userdmi = self.user_repository.get_userrole_by_uid("kw7l4xrZXEK")
        if userdmi is None:
            raise Exception("User Role dmi not found")
        try:
            authorities = [authority.authority for authority in userdmi.authorities]
            if self.M_BENCHMARK not in authorities:
                userdmi.authorities.append(
                    UserRoleAuthorities(
                        userroleid=userdmi.userroleid, authority=self.M_BENCHMARK
                    )
                )
            if self.M_FEEDBACK not in authorities:
                userdmi.authorities.append(
                    UserRoleAuthorities(
                        userroleid=userdmi.userroleid, authority=self.M_FEEDBACK
                    )
                )
            self.user_repository.save(userdmi)
            self.user_repository.commit()
        except Exception as e:
            logging.error(e)
            self.user_repository.rollback()

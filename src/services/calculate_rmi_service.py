import re
from datetime import datetime
from functools import cached_property
from typing import List, Union

from constants.rmi_const import (DATA_ELEMENT_RMI_SHORTNAMES,
                                 MAP_DATA_ELEMENT_RMI, RMI_INDEX_VALUE_SUMMARY)
from models.data_value import DataValue
from models.rmi import RmiModelsResult
from repositories.data_element_repository import DataElementRepository
from repositories.data_value_repository import DataValueRepository
from repositories.period_repository import PeriodRepository


class CalculateRMIService:
    def __init__(
        self,
        data_element_repository: DataElementRepository,
        data_value_repository: DataValueRepository,
        period_repository: PeriodRepository,
    ) -> None:
        self._data_value_repository = data_value_repository
        self._period_repository = period_repository
        self._data_element_repository = data_element_repository

    @cached_property
    def count_data_element(self) -> int:
        """
        Returns the number of data elements.
        :return: An integer representing the count of data elements.
        :rtype: int
        """
        return len(DATA_ELEMENT_RMI_SHORTNAMES)

    @staticmethod
    def data_value_to_int(value):
        """
        Converts a data value to an integer.
        Parameters:
            value (str): The data value to be converted.
        Returns:
            int: The converted integer value.
        """
        if value in ["true", "false"]:
            return 1 if value == "true" else 0
        return int(value)

    def get_data_values(self, org_unit_id: int) -> List[DataValue]:
        """
        Retrieves a list of data values based on the provided organization unit ID.
        Parameters:
            org_unit_id (int): The ID of the organization unit.
        Returns:
            List[DataValue]: A list of DataValue objects retrieved from the data value repository.
        """
        return self._data_value_repository.get_by_source_id(org_unit_id)

    def sort_data_values(
        self, data_values: List[DataValue]
    ) -> List[Union[int, None, bool]]:
        """
        Sorts the given list of DataValues based on their data element shortname.

        Parameters:
            data_values (List[DataValue]): A list of DataValue objects to be sorted.

        Returns:
            List[Union[int, None, bool]]: A sorted list of data values, where each value is either an integer, None, or a boolean.
        """
        # Sort the Data Values
        sorted_data_values = [None for i in range(len(DATA_ELEMENT_RMI_SHORTNAMES))]
        for data_val in data_values:
            # Regex condition
            regex_char = r"VII.[A-Z].[1-9].[1-9]"
            de_shortname: str = data_val.dataelement.shortname
            is_rme = re.match(regex_char, de_shortname)
            is_old_de = de_shortname.endswith("_Old")
            is_valid_de = (is_rme and not is_old_de) or de_shortname in ["I.A.1"]

            if is_valid_de and de_shortname in DATA_ELEMENT_RMI_SHORTNAMES:
                index = DATA_ELEMENT_RMI_SHORTNAMES.index(de_shortname)
                sorted_data_values[index] = data_val
        return [
            self.data_value_to_int(dv.value) if dv else None
            for dv in sorted_data_values
        ]

    def calculate_rmi(self, org_unit_id: int) -> Union[DataValue, None]:
        """
        Calculates the Risk of Mortality Index (RMI) for a given organization unit.
        Args:
            org_unit_id (int): The ID of the organization unit.
        Returns:
            Union[RmiModelsResult, None]: The calculated RMI value as an instance of RmiModelsResult, or None if there are no data values available.
        Raises:
            None.
        Example:
            rmi = calculate_rmi(1)
        """
        # 1. Get all data values for the organization unit
        data_values = self.get_data_values(org_unit_id)

        # 2. If there are no data values, return None
        if len(data_values) <= 0:
            return None

        # 3. Instantiate the RmiModelsResult
        rme_result = RmiModelsResult(org_unit_id)

        # 4. Sort the data values
        data_values = self.sort_data_values(data_values)

        # 5. Filter out the filled data values
        filled_data_value = list(filter(lambda d: d is not None, data_values))

        # 6. Calculate Percentage of filled data values
        percentage = len(filled_data_value) / self.count_data_element * 100
        rme_result.percentage = percentage

        # 7. If the percentage is less than or equal to 0, return RME with
        #    RMI value is 0
        if percentage <= 0:
            rme_result.data_element_uid = MAP_DATA_ELEMENT_RMI[0]
            return rme_result
        # 8. Calculate RME based on the filled data values
        result = 0
        for indx, key in enumerate(RMI_INDEX_VALUE_SUMMARY.keys()):
            # 9. Build the range index based on the key
            keys = [int(k) for k in key.split("-")]
            range_index = [i for i in range(keys[0], keys[1] + 1)]

            # 10. Get the minimum value based on the key
            min_value = RMI_INDEX_VALUE_SUMMARY[key]

            # 11. Sum the values in the range
            values = sum(
                list(filter(lambda d: d, [data_values[i] for i in range_index]))
            )

            # 12. Check if the values are less than the minimum value
            #     and update the result variable with the current index
            #     and break the loop
            if values < min_value:
                result = indx
                break
            # 13. If current index is the last index, update the result
            #     variable with the current index + 1
            if indx >= len(RMI_INDEX_VALUE_SUMMARY) - 1:
                result = indx + 1
        # 14. Set the data element uid based on the result
        rme_result.data_element_uid = MAP_DATA_ELEMENT_RMI[str(result)]
        # 15. Update the RME based on the result
        rme_result.rme = result

        return self.save_to_data_value(rme_result)

    def save_to_data_value(self, result: RmiModelsResult) -> Union[DataValue, None]:
        try:
            now = datetime.now()
            period = self._period_repository.get_by_year(now.year)
            if period is None:
                return None
            data_element = self._data_element_repository.get_by_uid(
                result.data_element_uid
            )
            data_value = self._data_value_repository.find_rme_data_values(
                result.rme, result.org_unit_id, period.periodid
            )
            if data_value is None:
                data_value = DataValue(
                    dataelementid=data_element.dataelementid,
                    sourceid=result.org_unit_id,
                    value=str(result.rme),
                )
            data_value.periodid = period.periodid
            data_value.storedby = "admin"
            data_value.categoryoptioncomboid = 24
            data_value.attributeoptioncomboid = 24
            # print(result.rme)
            self._data_value_repository.save(data_value)
            self._data_value_repository.commit()
            return data_value
        except Exception as e:
            print(e)
            return None

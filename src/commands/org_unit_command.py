import os
import click

from dependency_injector.wiring import inject, Provide
from services.orgunit_service import OrgUnitService
from container import Container
from osgeo import ogr


org_unit_commands = click.Group(name="org-unit")


@org_unit_commands.command(name="all-org-units")
@inject
def all_org_unit_command(orgunit_service: OrgUnitService = Provide[Container.orgunit_service]):
    orgunits = orgunit_service.get_all_org_units()
    for orgunit in orgunits:
        print(orgunit)


@org_unit_commands.command("read_gml_file", help="Place the gml file into datasource directory")
@click.option("--filename", default=None, help="Enter File name (check: datasource directory): ")
@inject
def read_gml_file(filename=None, orgunit_service: OrgUnitService = Provide[Container.orgunit_service]):
    if filename is None:
        raise Exception("Please enter file name")

    data_source_dir = os.path.dirname(os.path.abspath("./geo_extractor"))
    full_path = os.path.join(data_source_dir, "datasource", filename)
    if not os.path.exists(full_path):
        raise Exception("File not found")

    gml_data_source = ogr.Open(full_path)
    layer = gml_data_source.GetLayer(0)
    try:
        for feature in layer:
            # Access the attributes and geometry of each feature
            code = feature.GetField("CODE")
            name = feature.GetField("NAME")
            org_unit = orgunit_service.get_org_unit_by_code(code)
            if org_unit is None:
                print(name, code, "Wilayah Baru")
                continue
            geometry = feature.GetGeometryRef()
            geometry.FlattenTo2D()
            orgunit_service.update_geometry(org_unit, geometry.ExportToWkt())
            # print(org_unit.name, org_unit.code, code, name)
        orgunit_service.commit()
    except Exception as e:
        orgunit_service.rollback()
        raise (e)
    gml_data_source = None

import click
from dependency_injector.wiring import Provide, inject

from container import Container
from repositories.org_unit_repository import OrgUnitRepository
from services.calculate_rmi_service import CalculateRMIService

rmi_commands = click.Group(name="calculate-rmi")


@rmi_commands.command(name="calculate-rmi")
@inject
def calculate_rmi(
    org_unit_repository: OrgUnitRepository = Provide[Container.orgunit_repository],
    calculate_rmi_service: CalculateRMIService = Provide[
        Container.calculate_rmi_service
    ],
):
    orgunits = org_unit_repository.all()
    for orgunit in orgunits:
        rme_result = calculate_rmi_service.calculate_rmi(orgunit.organisationunitid)
        if rme_result:
            print(orgunit.name)
            print(rme_result)

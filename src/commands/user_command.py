import click
from dependency_injector.wiring import Provide, inject

from container import Container
from services.user_service import UserService

user_commands = click.Group(name="user")


@user_commands.command(name="all-users")
@inject
def all_users(
    user_service: UserService = Provide[Container.user_service],
):
    """
    Function to set all user makro to have konsensus.

    Args:
        user_service (UserService): The service for manipulating user data.
    """
    user_service.get_all_users()


@user_commands.command(name="set-user-makro-to-have-konsensus")
@inject
def set_user_makro_to_have_konsensus(
    user_service: UserService = Provide[Container.user_service],
):
    """
    Function to set all user makro to have konsensus.

    Args:
        user_service (UserService): The service for manipulating user data.
    """
    user_service.set_all_user_makro_to_have_konsensus()


@user_commands.command(name="set-user-mikro-to-have-indonesia")
@inject
def set_user_mikro_to_have_nasional(
    user_service: UserService = Provide[Container.user_service],
):
    """
    Function to set all user makro to have Indonesia Org Unit.

    Args:
        user_service (UserService): The service for manipulating user data.
    """
    user_service.set_all_user_mikro_to_have_nasional()


@user_commands.command(name="set-user-dmi-to-have-access-custom-app")
@inject
def set_user_dmi_to_have_access_custom_app(
    user_service: UserService = Provide[Container.user_service],
):
    """
    Function to set all user dmi to have access to custom app.

    Args:
        user_service (UserService): The service for manipulating user data.
    """
    user_service.set_user_dmi_to_have_access_custom_app()

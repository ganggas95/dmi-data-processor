from sqlalchemy import Column, ForeignKey, Integer, String, Table
from sqlalchemy.orm import mapped_column, relationship

from models.base import Base

dataelementgroupmembers = Table(
    "dataelementgroupmembers",
    Base.metadata,
    Column(
        "dataelementgroupid",
        ForeignKey("dataelementgroup.dataelementgroupid"),
        primary_key=True,
    ),
    Column("dataelementid", ForeignKey("dataelement.dataelementid"), primary_key=True),
)


class DataElementGroup(Base):
    __tablename__ = "dataelementgroup"
    dataelementgroupid = mapped_column("dataelementgroupid", Integer, primary_key=True)
    uid = mapped_column("uid", String)
    code = mapped_column("code", String)
    name = mapped_column("name", String)
    shortname = mapped_column("shortname", String)
    dataelements = relationship(
        "DataElement",
        back_populates="dataelementgroup",
        secondary=dataelementgroupmembers,
    )


class DataElement(Base):
    __tablename__ = "dataelement"
    dataelementid = mapped_column("dataelementid", Integer, primary_key=True)
    uid = mapped_column("uid", String)
    code = mapped_column("code", String)
    name = mapped_column("name", String)
    description = mapped_column("description", String)
    shortname = mapped_column("shortname", String)
    dataelementgroup = relationship(
        "DataElementGroup",
        back_populates="dataelements",
        # lazy='dynamic',
        secondary=dataelementgroupmembers,
    )

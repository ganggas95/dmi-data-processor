from dataclasses import dataclass
from typing import Union


@dataclass
class RmiModelsResult:
    org_unit_id: int
    percentage: float = 0
    rme: int = 0
    eligible: bool = False
    data_element_uid: Union[str, None] = None

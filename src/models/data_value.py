from sqlalchemy import Boolean, DateTime, ForeignKey, Integer, String, func
from sqlalchemy.orm import mapped_column, relationship

from models.base import Base


class DataValue(Base):
    __tablename__ = "datavalue"
    dataelementid = mapped_column(
        "dataelementid",
        Integer,
        ForeignKey("dataelement.dataelementid"),
        primary_key=True,
    )
    storedby = mapped_column(String, nullable=True)
    dataelement = relationship("DataElement", foreign_keys=[dataelementid])
    periodid = mapped_column(
        "periodid",
        Integer,
        # ForeignKey("period.periodid"),
        primary_key=True,
    )
    # period = relationship("Period", foreign_keys=[periodid])
    sourceid = mapped_column(
        "sourceid",
        Integer,
        # ForeignKey("organisationunit.organisationunitid"),
        primary_key=True,
    )
    # source = relationship("OrgUnit", foreign_keys=[sourceid])
    categoryoptioncomboid = mapped_column(
        "categoryoptioncomboid", Integer, primary_key=True
    )
    attributeoptioncomboid = mapped_column(
        "attributeoptioncomboid", Integer, primary_key=True
    )
    value = mapped_column(String)
    created = mapped_column(DateTime, default=func.now())
    lastupdated = mapped_column(
        DateTime, default=func.now(), onupdate=func.now()
    )
    deleted = mapped_column(Boolean, default=False)

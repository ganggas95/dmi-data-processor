from sqlalchemy import ForeignKey, Integer, String
from sqlalchemy.orm import mapped_column, relationship
from models.base import Base


class DataSet(Base):
    __tablename__ = "dataset"

    datasetid = mapped_column("datasetid", Integer, primary_key=True)
    code = mapped_column("code", String)
    name = mapped_column("name", String)
    shortname = mapped_column("shortname", String)


class DataSetElement(Base):
    __tablename__ = "datasetelement"

    datasetelementid = mapped_column("datasetelementid", Integer, primary_key=True)
    datasetid = mapped_column("datasetid", Integer, ForeignKey("dataset.datasetid"))
    dataset = relationship("DataSet", foreign_keys=[datasetid])
    dataelementid = mapped_column("dataelementid", Integer, ForeignKey("dataelement.dataelementid"))

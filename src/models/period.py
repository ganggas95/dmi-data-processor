from sqlalchemy import Date, ForeignKey, Integer, String
from sqlalchemy.orm import mapped_column

from models.base import Base


class PeriodType(Base):
    __tablename__ = "periodtype"
    periodtypeid = mapped_column("periodtypeid", Integer, primary_key=True)
    name = mapped_column("name", String)


class Period(Base):
    __tablename__ = "period"
    periodid = mapped_column("periodid", Integer, primary_key=True)
    periodtypeid = mapped_column(
        "periodtypeid", Integer, ForeignKey("periodtype.periodtypeid")
    )
    # periodtype = relationship("PeriodType", foreign_keys=[periodtypeid])
    startdate = mapped_column("startdate", Date)
    enddate = mapped_column("enddate", Date)

from geoalchemy2 import Geometry
from sqlalchemy import Integer, String
from sqlalchemy.orm import mapped_column

from models.base import Base


class OrgUnit(Base):
    __tablename__ = 'organisationunit'
    organisationunitid = mapped_column('organisationunitid', Integer, primary_key=True)
    uid = mapped_column('uid', String)
    code = mapped_column('code', String)
    name = mapped_column('name', String)
    hierarchylevel = mapped_column('hierarchylevel', Integer)
    shortname = mapped_column('shortname', String)
    parentid = mapped_column('parentid', Integer)
    geometry = mapped_column('geometry', Geometry)

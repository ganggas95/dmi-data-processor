from sqlalchemy import UUID, ForeignKey, Integer, String
from sqlalchemy.orm import mapped_column, relationship

from models.base import Base


class UserGroup(Base):
    __tablename__ = "usergroup"
    usergroupid = mapped_column("usergroupid", Integer, primary_key=True)
    uid = mapped_column("uid", String)
    code = mapped_column("code", String)
    name = mapped_column("name", String)
    # userid = mapped_column("userid", Integer, ForeignKey("UserInfo.userinfoid"))
    # user = relationship("UserInfo", foreign_keys=[userid])
    publicaccess = mapped_column("publicaccess", String)
    uuid = mapped_column("uuid", UUID)
    users = relationship(
        "UserInfo",
        secondary="usergroupmembers",
        back_populates="usergroups",
        primaryjoin="UserGroup.usergroupid == UserGroupMembers.usergroupid",
        secondaryjoin="UserGroupMembers.userinfoid == UserInfo.userinfoid",
    )


class UserGroupMembers(Base):
    __tablename__ = "usergroupmembers"
    usergroupid = mapped_column("usergroupid", Integer, primary_key=True)
    userinfoid = mapped_column("userid", Integer, primary_key=True)


class UserRole(Base):
    __tablename__ = "userrole"
    userroleid = mapped_column("userroleid", Integer, primary_key=True)
    # userid = mapped_column("userid", Integer, ForeignKey("UserInfo.userinfoid"))
    # user = relationship("UserInfo", foreign_keys=[userid])
    uid = mapped_column("uid", String)
    code = mapped_column("code", String)
    name = mapped_column("name", String)
    description = mapped_column("description", String)
    publicaccess = mapped_column("publicaccess", String)
    users = relationship(
        "UserInfo",
        secondary="userrolemembers",
        back_populates="userroles",
        primaryjoin="UserRole.userroleid == UserRoleMembers.userroleid",
        secondaryjoin="UserRoleMembers.userinfoid == UserInfo.userinfoid",
    )
    authorities = relationship(
        "UserRoleAuthorities",
        back_populates="userrole",
        foreign_keys="UserRoleAuthorities.userroleid",
    )


class UserRoleMembers(Base):
    __tablename__ = "userrolemembers"
    userroleid = mapped_column("userroleid", Integer, primary_key=True)
    userinfoid = mapped_column("userid", Integer, primary_key=True)


class UserInfo(Base):
    __tablename__ = "userinfo"
    userinfoid = mapped_column("userinfoid", Integer, primary_key=True)
    uid = mapped_column("uid", String)
    surname = mapped_column("surname", String)
    firstname = mapped_column("firstname", String)
    email = mapped_column("email", String)
    phonenumber = mapped_column("phonenumber", String)
    jobtitle = mapped_column("jobtitle", String)
    uuid = mapped_column("uuid", UUID)
    username = mapped_column("username", String)
    password = mapped_column("password", String)

    usergroups = relationship(
        "UserGroup",
        secondary="usergroupmembers",
        back_populates="users",
        primaryjoin="UserInfo.userinfoid == UserGroupMembers.userinfoid",
        secondaryjoin="UserGroupMembers.usergroupid == UserGroup.usergroupid",
    )
    userroles = relationship(
        "UserRole",
        secondary="userrolemembers",
        back_populates="users",
        primaryjoin="UserInfo.userinfoid == UserRoleMembers.userinfoid",
        secondaryjoin="UserRoleMembers.userroleid == UserRole.userroleid",
        uselist=True,
    )
    # dataview_org_units = relationship(
    #     "OrgUnit",
    #     secondary="userdatavieworgunits",
    #     primaryjoin="UserInfo.userinfoid == UserDataViewOrgUnits.userinfoid",
    #     secondaryjoin="OrgUnit.organisationunitid == UserDataViewOrgUnits.id",
    #     uselist=True,
    # )


class UserDataViewOrgUnits(Base):
    __tablename__ = "userdatavieworgunits"
    userinfoid = mapped_column("userinfoid", Integer, primary_key=True)
    id = mapped_column("id", Integer, primary_key=True)


class UserRoleAuthorities(Base):
    __tablename__ = "userroleauthorities"
    userroleid = mapped_column(
        "userroleid", Integer, ForeignKey("userrole.userroleid"), primary_key=True
    )
    userrole = relationship("UserRole", foreign_keys=[userroleid])
    authority = mapped_column("authority", String, primary_key=True)
